/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cr.controlador;

import cr.modelo.Jugador;
import cr.modelo.RegistroJugador;
import cr.vista.GUIPrincipal;
import cr.vista.GUIPuntuaciones;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Diana Artavia
 */
public class ControladorPuntuaciones implements ActionListener{
    private GUIPuntuaciones guiPuntuaciones;
    private GUIPrincipal guiPrincipal;
    private RegistroJugador registroJugador;
    private Jugador jugador;

    public ControladorPuntuaciones(GUIPuntuaciones guiPuntuaciones, GUIPrincipal guiPrincipal, RegistroJugador registroJugador) {
        this.guiPrincipal = guiPrincipal;
        this.guiPuntuaciones = guiPuntuaciones;
        this.registroJugador= registroJugador;
    }//fin del constructor

    @Override
    public void actionPerformed(ActionEvent evento) {
        if (evento.getActionCommand().equals("Volver")) {
           guiPuntuaciones.setVisible(false);
            guiPrincipal.setVisible(true);
        }//fin del if
        
         if (evento.getActionCommand().equals("Borrar")) {
          registroJugador.reiniciarPuntuacion(guiPrincipal.getJlNombreUsuario());
          guiPuntuaciones.setDatosTabla(registroJugador.getDatosTabla(), Jugador.ETIQUETAS_JUGADOR);
        }//fin del if
    }//fin del actionPerformed
    
}//fin de la clase
