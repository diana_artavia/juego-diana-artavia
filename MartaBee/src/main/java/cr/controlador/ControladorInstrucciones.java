/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cr.controlador;

import cr.vista.GUIInstrucciones;
import cr.vista.GUIPrincipal;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Action;

/**
 *
 * @author Diana Artavia
 */
public class ControladorInstrucciones implements ActionListener {

    private GUIInstrucciones guiInstrucciones;
    private GUIPrincipal guiPrincipal;
    
    public ControladorInstrucciones(GUIInstrucciones guiInstrucciones, GUIPrincipal guiPrincipal) {
        this.guiInstrucciones = guiInstrucciones;
        this.guiPrincipal = guiPrincipal;
        
    }//fin del constructor

    @Override
    public void actionPerformed(ActionEvent evento) {
        if (evento.getActionCommand().equals("Volver")) {
            guiInstrucciones.setVisible(false);
            guiPrincipal.setVisible(true);
        }//fin del if
    }//fin del actionPerformed

}//fin de la clase
