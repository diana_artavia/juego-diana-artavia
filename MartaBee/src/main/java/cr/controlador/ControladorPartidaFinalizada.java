/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cr.controlador;

import cr.modelo.AreaJuego;
import cr.vista.GUIJuego;
import cr.vista.GUIPartidaFinalizada;
import cr.vista.GUIPrincipal;
import cr.vista.PanelJuego;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Diana Artavia
 */
public class ControladorPartidaFinalizada implements ActionListener {

    private GUIPartidaFinalizada guiPartidaFinalizada;
    private GUIPrincipal guiPrincipal;
    private GUIJuego guiJuego;
    private ControladorGUIJuego controladorGUIJuego;

    public ControladorPartidaFinalizada(GUIPartidaFinalizada guiPartidaFinalizada, GUIPrincipal guiPrincipal, GUIJuego guiJuego, ControladorGUIJuego controladorGUIJuego) {
        this.guiPartidaFinalizada = guiPartidaFinalizada;
        this.guiPrincipal = guiPrincipal;
        this.guiJuego = guiJuego;
        this.controladorGUIJuego = controladorGUIJuego;
    }//fin del constructor

    @Override
    public void actionPerformed(ActionEvent evento) {
        switch (evento.getActionCommand()) {
            case "Jugar":
                controladorGUIJuego.getAreaJuego().getPersonajePrincipal().setVidas(3);
                controladorGUIJuego.getAreaJuego().reiniciarPuntos();
                guiJuego.setVisible(true);
                controladorGUIJuego.crearHilo();
                controladorGUIJuego.getAreaJuego().setUbicacion();
                controladorGUIJuego.getHiloJuego().start();
                break;
            case "Inicio":
                guiPrincipal.setVisible(true);
                guiPartidaFinalizada.setVisible(false);
                break;
        }//fin del switch
    }//fin del actionPerformed

}//fin de la clase
