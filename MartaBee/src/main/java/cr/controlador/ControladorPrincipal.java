/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cr.controlador;

import cr.modelo.Jugador;
import cr.modelo.Musica;
import cr.modelo.RegistroJugador;
import cr.vista.GUICreditos;
import cr.vista.GUIInicioSesion;
import cr.vista.GUIInstrucciones;
import cr.vista.GUIJuego;
import cr.vista.GUIPartidaFinalizada;
import cr.vista.GUIPrincipal;
import cr.vista.GUIPuntuaciones;
import cr.vista.PanelJuego;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Diana Artavia
 */
public class ControladorPrincipal implements ActionListener {

    private GUIPrincipal guiPrincipal;
    private GUIInstrucciones guiInstrucciones;
    private GUICreditos guiCreditos;
    private GUIInicioSesion guiInicioSesion;
    private GUIPuntuaciones guiPuntuaciones;
    private GUIJuego guiJuego;
    private GUIPartidaFinalizada guiPartidaFinalizada;
    private ControladorGUIJuego controladorGUIJuego;
    private PanelJuego panelJuego;
    private RegistroJugador registroJugador;
    private Musica musica;

    public ControladorPrincipal(GUIPrincipal guiPrincipal) {
        musica = new Musica("cancionPrincipal");
        musica.reproducirMusica();
        musica.loop();
        this.guiPrincipal = guiPrincipal;
        guiJuego = new GUIJuego();
        guiInstrucciones = new GUIInstrucciones(guiPrincipal);
        guiCreditos = new GUICreditos(guiPrincipal);
        guiInicioSesion = new GUIInicioSesion(guiPrincipal);
        panelJuego = guiJuego.getPanelJuego();
        controladorGUIJuego = panelJuego.getControladorGUIJuego();
        guiPartidaFinalizada = new GUIPartidaFinalizada(guiPrincipal, guiJuego, controladorGUIJuego);
        guiJuego.setControlador(controladorGUIJuego);
        controladorGUIJuego.setGUIJuego(guiJuego);
        controladorGUIJuego.setGUIPartidaFinalizada(guiPartidaFinalizada);
        controladorGUIJuego.setGUIPrincipal(guiPrincipal);
        registroJugador = guiInicioSesion.getControlador().getRegistroJugador();
        controladorGUIJuego.setRegistroJugador(registroJugador);
        guiPuntuaciones = new GUIPuntuaciones(guiPrincipal, registroJugador);
    }//fin del ControladorPrincipal

    @Override
    public void actionPerformed(ActionEvent evento) {
        String eventoEscuchado = evento.getActionCommand();

        switch (eventoEscuchado) {
            case "Jugar":
                controladorGUIJuego.crearHilo();
                controladorGUIJuego.getAreaJuego().reiniciarPuntos();
                controladorGUIJuego.getAreaJuego().getPersonajePrincipal().setVidas(3);
                controladorGUIJuego.getAreaJuego().setUbicacion();
                controladorGUIJuego.getHiloJuego().start();
                guiPrincipal.setVisible(false);
                panelJuego.repaint();
                guiJuego.setVisible(true);
                break;

            case "Instrucciones":
                guiPrincipal.setVisible(false);
                guiInstrucciones.setVisible(true);
                break;

            case "Puntuaciones":
                guiPuntuaciones.setDatosTabla(registroJugador.getDatosTabla(), Jugador.ETIQUETAS_JUGADOR);
                guiPrincipal.setVisible(false);
                guiPuntuaciones.setVisible(true);
                break;

            case "Creditos":
                guiPrincipal.setVisible(false);
                guiCreditos.setVisible(true);
                break;

            case "Salir":
                System.exit(0);
                break;

            case "Sesion":
                guiPrincipal.setVisible(false);
                guiInicioSesion.visibilidadMenuInicioSesion(true);
                guiInicioSesion.setVisible(true);
                break;

            case "Silenciar":
                musica.stopMusica();
                guiPrincipal.getJbSilenciarMusica().setVisible(false);
                guiPrincipal.getJbActivarMusica().setVisible(true);
                break;

            case "Activar":
                musica.reproducirMusica();
                guiPrincipal.getJbActivarMusica().setVisible(false);
                guiPrincipal.getJbSilenciarMusica().setVisible(true);
                break;
        }//fin del switch
    }//fin del actionPerformed

}//fin de la clase
