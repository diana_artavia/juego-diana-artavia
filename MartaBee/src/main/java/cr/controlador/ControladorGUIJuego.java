/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cr.controlador;

import cr.modelo.AreaJuego;
import cr.modelo.RegistroJugador;
import cr.vista.GUIJuego;
import cr.vista.GUIPartidaFinalizada;
import cr.vista.GUIPrincipal;
import cr.vista.PanelJuego;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

/**
 *
 * @author Diana Artavia
 */
public class ControladorGUIJuego implements KeyListener {

    private GUIJuego guiJuego;
    private PanelJuego panelJuego;
    private AreaJuego areaJuego;
    private HiloJuego hiloJuego;
    private GUIPartidaFinalizada guiPartidaFinalizada;
    private GUIPrincipal guiPrincipal;
    private RegistroJugador registroJugador;
    private ArrayList<HiloJuego>listaHilos;

    public ControladorGUIJuego (PanelJuego panelJuego) {
        areaJuego = new AreaJuego();
        this.panelJuego = panelJuego;
        panelJuego.setControlador(this);
        }//fin del constructor
    
    public void crearHilo() {
        hiloJuego = new HiloJuego(areaJuego, panelJuego, guiJuego, this);
        areaJuego.setVelocidad();
        panelJuego.setJlPuntos(0);
        
    }//fin del metodo crearHilo

    public HiloJuego getHiloJuego(){
        return hiloJuego;
    }//fin del metodo crearHilo

    public ArrayList<HiloJuego> getListaHilos() {
        return listaHilos;
    }//fin del metodo getListaHilos
    
    public void setGUIJuego(GUIJuego guiJuego) {
        this.guiJuego = guiJuego;
    }//fin del metodo setGUIJuego

    public void setGUIPartidaFinalizada(GUIPartidaFinalizada guiPartidaFinalizada) {
        this.guiPartidaFinalizada = guiPartidaFinalizada;
    }//fin del metodo setGUIPartidaFinalizada

    public void setGUIPrincipal(GUIPrincipal guiPrincipal) {
        this.guiPrincipal = guiPrincipal;
    }//fin del metodo setGUIPrincipal

    public void setRegistroJugador(RegistroJugador registroJugador) {
        this.registroJugador = registroJugador;
    }//fin del metodo setRegistroJugador

    public AreaJuego getAreaJuego() {
        return areaJuego;
    }//fin del getAreaJuego

    public void dibujar(Graphics g) {
        areaJuego.getFondo().dibujar(g);

        if (areaJuego.getPersonajePrincipal().getVidas() == 3) {
            areaJuego.getPrimerVida().dibujar(g);
            areaJuego.getSegundaVida().dibujar(g);
            areaJuego.getTercerVida().dibujar(g);
        }
        if (areaJuego.getPersonajePrincipal().getVidas() == 2) {
            areaJuego.getSegundaVida().dibujar(g);
            areaJuego.getTercerVida().dibujar(g);
        }
        if (areaJuego.getPersonajePrincipal().getVidas() == 1) {
            areaJuego.getTercerVida().dibujar(g);
        }
        for (int posicion = 0; posicion < areaJuego.getCantidadEnemigos(); posicion++) {
            areaJuego.getEnemigos(posicion).dibujar(g);
        }
        for (int posicion = 0; posicion < areaJuego.getCantidadAliados(); posicion++) {
            areaJuego.getAliados(posicion).dibujar(g);
        }
        areaJuego.getPersonajePrincipal().dibujar(g);
    }//fin del metodo dibujar

    public void actualizar() {
        panelJuego.repaint();
    }//fin del metodo actualizar

    public void juegoFinalizado() {
        guiJuego.dispose();
        guiPartidaFinalizada.setJlPuntuacion(areaJuego.puntosTotales());
        setPuntuacion();
        guiPartidaFinalizada.setVisible(true);
        listaHilos.remove(0);
    } //fin del metodo juegoFinalizado

    public void setPuntuacion() {
       // if (registroJugador.getPuntuacionAlta(guiPrincipal.getJlNombreUsuario(), String.valueOf(areaJuego.puntosTotales()))) {
            registroJugador.agregarPuntuacion(String.valueOf(areaJuego.puntosTotales()), guiPrincipal.getJlNombreUsuario());
            registroJugador.escribirJSON();
        //}//fin del if
    }//fin del metodo setPuntuacion
    
    public RegistroJugador getRegistroJugador(){
        return registroJugador;
    }

    public GUIPrincipal getGuiPrincipal(){
        return guiPrincipal;
    }
    
    public String getPuntosTotales() {
        String puntosTotales = String.valueOf(areaJuego.puntosTotales());
        return puntosTotales;
    }//fin del metodo getPuntosTotales

    @Override
    public void keyTyped(KeyEvent evento) {
    }//fin del metodo keyTyped
    @Override
    public void keyPressed(KeyEvent evento) {
        if (evento.getKeyCode() == KeyEvent.VK_W) {
            up();
        } else {
            if (evento.getKeyCode() == KeyEvent.VK_S) {
                down();
            } else {
                if (evento.getKeyCode() == KeyEvent.VK_A) {
                    left();
                } else {
                    if (evento.getKeyCode() == KeyEvent.VK_D) {
                        right();
                    }//fin del if
                }//fin del if/else
            }//fin del if/else
        }//fin del if/else
        actualizar();
    }//fin del metodo keyPressed

    @Override
    public void keyReleased(KeyEvent evento) {
    }//fin del metodo keyReleased

    public void up() {
        areaJuego.getPersonajePrincipal().setY(areaJuego.getPersonajePrincipal().getY() - 20);
    }//fin del metodo up

    public void down() {
        areaJuego.getPersonajePrincipal().setY(areaJuego.getPersonajePrincipal().getY() + 20);
    }//fin del metodo down

    public void left() {
        areaJuego.getPersonajePrincipal().setX(areaJuego.getPersonajePrincipal().getX() - 10);
    }//fin del metodo left

    public void right() {
        areaJuego.getPersonajePrincipal().setX(areaJuego.getPersonajePrincipal().getX() + 10);
    }//fin del metodo right
}//fin de la clase
