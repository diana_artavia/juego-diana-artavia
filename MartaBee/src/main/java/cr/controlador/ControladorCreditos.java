/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cr.controlador;

import cr.vista.GUICreditos;
import cr.vista.GUIPrincipal;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Diana Artavia
 */
public class ControladorCreditos implements ActionListener{
    
    private GUICreditos guiCreditos;
    private GUIPrincipal guiPrincipal;
    
    public ControladorCreditos(GUICreditos guiCreditos, GUIPrincipal guiPrincipal){
        this.guiCreditos = guiCreditos;
        this.guiPrincipal = guiPrincipal;
    }//fin del constructor

    @Override
    public void actionPerformed(ActionEvent evento) {
        if (evento.getActionCommand().equals("Volver")) {
            guiCreditos.setVisible(false);
            guiPrincipal.setVisible(true);
        }//fin del if
    }//fin del actionPerformed
    
}//fin de la clase
