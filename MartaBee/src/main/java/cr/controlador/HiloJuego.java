/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cr.controlador;

import cr.modelo.AreaJuego;
import cr.vista.GUIJuego;
import cr.vista.PanelJuego;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Diana Artavia
 */
public class HiloJuego extends Thread {

    private AreaJuego areaJuego;
    private PanelJuego panelJuego;
    private GUIJuego guiJuego;
    private ControladorGUIJuego controlador;

    public HiloJuego(AreaJuego areaJuego, PanelJuego panelJuego, GUIJuego guiJuego, ControladorGUIJuego controlador) {
        this.areaJuego = areaJuego;
        this.panelJuego = panelJuego;
        this.guiJuego = guiJuego;
        this.controlador = controlador;
    }//fin del constructor con parametros

    public void run() {
        while (areaJuego.getPersonajePrincipal().getVidas() > 0) {
            try {
                for (int posicion = 0; posicion < areaJuego.getCantidadEnemigos(); posicion++) {
                    areaJuego.getEnemigos(posicion).movimiento();
                    if (areaJuego.colisionEnemigo()) {
                        if (areaJuego.partidaFinalizadas()) {
                            controlador.juegoFinalizado();
                        }//fin del if vidas
                    }//fin del if colision
                    controlador.actualizar();
                } //fin del for enemigo
                for (int posicion = 0; posicion < areaJuego.getCantidadAliados(); posicion++) {
                    areaJuego.getAliados(posicion).movimiento();
                    areaJuego.colisionAliado();
                    panelJuego.setJlPuntos(areaJuego.puntosTotales());
                    controlador.actualizar();
                }//fin del for aliado
                sleep(areaJuego.getVelocidad());
            }//fin del try
            catch (InterruptedException ex) {
                Logger.getLogger(HiloJuego.class.getName()).log(Level.SEVERE, null, ex);
            }//fin del catch
        } //fin del while
    }//fin del metodo run
}//fin de la clase
