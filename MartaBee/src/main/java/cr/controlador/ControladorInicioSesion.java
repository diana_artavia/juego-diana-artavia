/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cr.controlador;

import cr.modelo.Jugador;
import cr.modelo.RegistroJugador;
import cr.vista.GUIInicioSesion;
import cr.vista.GUIPrincipal;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Diana Artavia
 */
public class ControladorInicioSesion implements ActionListener {

    private GUIInicioSesion guiInicioSesion;
    private GUIPrincipal guiPrincipal;
    private Jugador jugador;
    private RegistroJugador registroJugador;

    public ControladorInicioSesion(GUIInicioSesion guiInicioSesion, GUIPrincipal guiPrincipal) {
        this.guiInicioSesion = guiInicioSesion;
        this.guiPrincipal = guiPrincipal;
        registroJugador = new RegistroJugador();
    }//fin del constructor
    
    public RegistroJugador getRegistroJugador(){
        return registroJugador;
    }//fin del metodo getRegistroJugador

    @Override
    public void actionPerformed(ActionEvent evento) {
        String nombre = guiInicioSesion.getTxtNombreUsuario();
        String eventoEscuchado = evento.getActionCommand();

        switch (eventoEscuchado) {
            case "Crear cuenta":
                guiInicioSesion.visibilidadMenuInicioSesion(false);
                guiInicioSesion.visibilidadCrearUsuario(true);
                break;
            case "Volver":
                guiInicioSesion.visibilidadMenuInicioSesion(true);
                guiInicioSesion.visibilidadCrearUsuario(false);
                break;

            case "Confirmar":
                guiInicioSesion.dispose();
                jugador = registroJugador.buscar(nombre);
                if (jugador == null) {
                    jugador = new Jugador(nombre, "0");
                    registroJugador.agregar(jugador);
                }
                guiInicioSesion.limpiarDatos();
                guiPrincipal.setJlNombreUsuario(nombre);
                System.out.println(jugador + " " + nombre);
                guiPrincipal.setVisible(true);
                break;

            case "Iniciar sesion":
                guiInicioSesion.visibilidadMenuInicioSesion(false);
                guiInicioSesion.visibilidadIniciarSesion(true);
                break;

            case "Volver inicio":
                guiInicioSesion.dispose();
                guiPrincipal.setVisible(true);
                break;

            case "Cerrar sesion":
                guiPrincipal.setVisible(true);
                guiPrincipal.setJlNombreUsuario("Iniciar sesión");
                break;

        }//fin del switch

    }//fin del actionPerformed

}//fin de la clase
