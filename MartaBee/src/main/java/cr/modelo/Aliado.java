/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cr.modelo;

import javax.swing.ImageIcon;

/**
 *
 * @author Diana Artavia
 */
public class Aliado extends Elemento {

    private int direccion;
    private int puntos;
    private boolean colision;

    public Aliado(ImageIcon imagen, int x, int y, int direccion) {
        super(imagen, x, y);
        this.direccion = direccion;
        this.colision = true;
    }//fin del constructor

    public Aliado(ImageIcon imagen, int x, int y) {
        super(imagen, x, y);
    }//fin del constructor

    public int getDireccion() {
        return direccion;
    }//fin del metodo getDireccion

    public void setDireccion(int direccion) {
        this.direccion = direccion;
    }//fin del metodo setDireccion

    public boolean isColision() {
        return colision;
    }//fin del metodo isColision

    public void setColision(boolean colision) {
        this.colision = colision;
    }//fin del metodo setColision

    public int getPuntos() {
        return puntos;
    }//fin del metodo getPuntos

    public void setPuntos(int puntos) {
        this.puntos = puntos;
    }//fin del metodo setPuntos

    public void movimiento() {
        if (getX() > 0) {
            setX(getX() - 20);
        } else {
            setX(1200);
        }//fin del if/else
    }//fin del metodo movimiento   

    public void puntosFlor(int numero) {
        switch (numero) {
            case 0:
                setPuntos(21);
                break;
            case 1:
                setPuntos(9);
                break;
            case 2:
                setPuntos(15);
                break;
            case 3:
                setPuntos(3);
                break;
            case 4:
                setPuntos(32);
                break;
        }//fin del switch
    }//fin del metodo seleccionarFlor

    public void ubicacion(int num) {
        if (num == 0) {
            setY(150);
            setX(2200);
            setColision(true);
        }
        if (num == 1) {
            setY(320);
            setX(1500);
        }
        if (num == 2) {
            setY(450);
            setX(1350);
            setColision(true);
        }

        if (num == 3) {
            setY(520);
            setX(1100);
            setColision(true);
        }

        if (num == 4) {
            setY(700);
            setX(1700);
            setColision(true);
        }
        if (num == 5) {
            setY(620);
            setX(2500);
            setColision(true);
        }
        if (num == 6) {
            setY(850);
            setX(1900);
            setColision(true);
        }
    }//fin del metodo posicion
}//fin de la clase
