/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cr.modelo;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;

/**
 *
 * @author Diana Artavia
 */
public class Musica {

    private Clip sonido;

    public Musica(String nombreCancion) {
        crearMusica(nombreCancion);
    }//fin del constructor

    public void crearMusica(String nombreCancion) {
        try {
            sonido = AudioSystem.getClip();
            sonido.open(AudioSystem.getAudioInputStream(new File("./src/main/resources/" + nombreCancion + ".wav")));

        } catch (Exception ex) {
            System.out.println(ex);
        }//fin del try/catch

    }//fin del metodo reproducirMusica

    public void reproducirMusica() {
        sonido.start();
    }

    public void stopMusica() {
        sonido.stop();
    }//fin del metodo stopMusica

    public void loop() {
        sonido.loop(Clip.LOOP_CONTINUOUSLY);
    }//fin del metodo loop

}//fin de la clase
