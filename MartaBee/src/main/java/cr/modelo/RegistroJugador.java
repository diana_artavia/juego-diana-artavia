/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cr.modelo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author Diana Artavia
 */
public class RegistroJugador {

    private ArrayList<Jugador> listaJugadores;
    private JSONObject baseJSONJugador;
    private File archivo;

    public RegistroJugador() {
        listaJugadores = new ArrayList<>();
        archivo = new File("jugadores.json");
        leerJSON();
    }//fin del constructor

    public void escribirJSON() {
        JSONArray arregloJugador = new JSONArray();
        baseJSONJugador = new JSONObject();
        for (Jugador jugador : listaJugadores) {
            JSONObject objJSONJugador = new JSONObject();
            objJSONJugador.put("nombre", jugador.getNombre());
            objJSONJugador.put("puntuacion", jugador.getPuntuacion());
            arregloJugador.add(objJSONJugador);
        }//fin del for
        baseJSONJugador.put("listaJugadores", arregloJugador);

        try {
            FileWriter escribir = new FileWriter(archivo);
            escribir.write(baseJSONJugador.toJSONString());
            escribir.flush();
            escribir.close();
        } catch (IOException ex) {
            System.err.println("Error al escribir el archivo");
        }//fin del try/catch
    }//fin del metodo escribirJSON

    public void leerJSON() {
        JSONParser parser = new JSONParser();
        try {
            FileReader leer = new FileReader(archivo);
            Object obj = parser.parse(leer);
            baseJSONJugador = (JSONObject) obj;
            JSONArray arregloJSON = (JSONArray) baseJSONJugador.get("listaJugadores");
            for (Object object : arregloJSON) {
                JSONObject objJugador = (JSONObject) object;
                Jugador jugador = new Jugador();
                jugador.setNombre(objJugador.get("nombre").toString());
                jugador.setPuntuacion(objJugador.get("puntuacion").toString());
                listaJugadores.add(jugador);
            }//fin del for
        } catch (FileNotFoundException ex) {
            System.err.println("Archivo no encontrado");
        } catch (IOException ex) {
            System.err.println("Error al leer");
        } catch (ParseException ex) {
            ex.printStackTrace();
        }//fin del try/catch
    }//fin del metodo leerJSON

    public void agregar(Jugador jugador) {
        listaJugadores.add(jugador);
        escribirJSON();
    }//fin del metodo agregar

    public Jugador buscar(String nombre) {
        for (int posicion = 0; posicion < listaJugadores.size(); posicion++) {
            if (listaJugadores.get(posicion).getNombre().equals(nombre)) {
                return listaJugadores.get(posicion);
            }//fin del if 
        }//fin del for
        return null;
    }//fin del metodo buscar

    public boolean isPuntuacionAlta(String nombre, String puntuacion) {
        for (int posicion = 0; posicion < listaJugadores.size(); posicion++) {
            if (listaJugadores.get(posicion).getNombre().equals(nombre)) {
                if(!listaJugadores.get(posicion).getPuntuacion().equals(puntuacion)){
                   if(Integer.parseInt(listaJugadores.get(posicion).getPuntuacion())<Integer.parseInt(puntuacion)){
                       return true;
                   }
                }
            }//fin del if 
        }//fin del for
        return false;
    }//fin del metodo getPuntuacionAlta

    public void agregarPuntuacion(String puntacion, String nombre) {
        for (int posicion = 0; posicion < listaJugadores.size(); posicion++) {
            if (listaJugadores.get(posicion).getNombre().equals(nombre)) {
                listaJugadores.get(posicion).setPuntuacion(puntacion);
                escribirJSON();
            }//fin del if 
        }//fin del for
    }//fin del metodo modificarPuntuacion
    
    public void reiniciarPuntuacion(String nombre){
        for (int posicion = 0; posicion < listaJugadores.size(); posicion++) {
            if (listaJugadores.get(posicion).getNombre().equals(nombre)) {
                listaJugadores.get(posicion).setPuntuacion("0");
                escribirJSON();
            }//fin del if 
        }//fin del for
    }//fin del metodo reiniciarPuntuacion

    public String[][] getDatosTabla() {
        String[][] matrizTabla = new String[listaJugadores.size()][Jugador.ETIQUETAS_JUGADOR.length];
        for (int fila = 0; fila < listaJugadores.size(); fila++) {
            for (int columna = 0; columna < matrizTabla[0].length; columna++) {
                matrizTabla[fila][columna] = listaJugadores.get(fila).getDatosJugador(columna);
            }//fin del for columna
        }//fin del for fila
        return matrizTabla;
    }//fin del metodo getDatosTabla

    public String[] getPuntuaciones() {
        String[] listaPuntuaciones = new String[listaJugadores.size()];
        for (int indice = 0; indice < listaJugadores.size(); indice++) {
            listaPuntuaciones[indice] = listaJugadores.get(indice).getPuntuacion();
        }//fin del for  
        return listaPuntuaciones;
    }//fin del metodo getPuntuaciones

    @Override
    public String toString() {
        String listadoJugadores = "";
        for (int posicion = 0; posicion < listaJugadores.size(); posicion++) {
            listadoJugadores += listaJugadores.get(posicion).toString() + "\n---------\n";
        }//fin del for
        return listadoJugadores;
    }//fin del toString

}//fin de la clase 
