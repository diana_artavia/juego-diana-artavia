/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cr.modelo;

import javax.swing.ImageIcon;

/**
 *
 * @author Diana Artavia
 */
public class Enemigo extends Elemento {

    private int direccion;
    private boolean colision;

    public Enemigo(ImageIcon imagen, int x, int y, int direccion) {
        super(imagen, x, y);
        this.direccion = direccion;
        this.colision = true;
    }//fin del constructor

    public Enemigo(ImageIcon imagen, int x, int y) {
        super(imagen, x, y);
    }

    public int getDireccion() {
        return direccion;
    }//fin del metodo getDireccion

    public void setDireccion(int direccion) {
        this.direccion = direccion;
    }//fin del metodo setDireccion

    public boolean isColision() {
        return colision;
    }//fin del metodo isColision

    public void setColision(boolean colision) {
        this.colision = colision;
    }//fin del metodo setColision

    public void movimiento() {
        switch (direccion) {
            case 0:
                if (getX() > 0) {
                    setX(getX() - 20);
                } else {
                    setX(1200);
                }//fin del if/else
                break;
        }//fin del switch
    }//fin del metodo movimiento

    public void ubicacion(int num) {
        if (num == 0) {
            setY(200);
            setX(2000);
        }
        if (num == 1) {
            setY(400);
            setX(1200);
        }
        if (num == 2) {
            setY(600);
            setX(1400);
        }

        if (num == 3) {
            setY(100);
            setX(1600);
        }

        if (num == 4) {
            setY(750);
            setX(1800);
        }
    }//fin del metodo posicion

}//fin de la clase
