/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cr.modelo;

import java.awt.Graphics;
import javax.swing.ImageIcon;

/**
 *
 * @author Diana Artavia
 */
public class Elemento {
    
    private ImageIcon imagen;
    private int x;
    private int y;

    public Elemento(ImageIcon imagen, int x, int y) {
        this.imagen = imagen;
        this.x = x;
        this.y = y;
    }//fin del constructor

    public Elemento(int x, int y) {
        this.x = x;
        this.y = y;
    }//fin del constructor

    public void setImagen(ImageIcon imagen) {
        this.imagen = imagen;
    }//fin del metodo setImagen
    
    public ImageIcon getImagen() {
        return imagen;
    }//fin del metodo getImagen

    public void setX(int x) {
        this.x = x;
    }//fin del metodo setX
    
     public int getX() {
        return x;
    }//fin del metodo getX

    public void setY(int y) {
        this.y = y;
    }//fin del metodo setY
    
    public int getY() {
        return y;
    }//fin del metodo getY

    public void dibujar (Graphics g){
        imagen.paintIcon(null, g, getX(), getY());
    }//fin del metodo dibujar
}//fin de la clase
