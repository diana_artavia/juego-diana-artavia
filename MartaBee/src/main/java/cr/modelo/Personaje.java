/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cr.modelo;

import javax.swing.ImageIcon;

/**
 *
 * @author Diana Artavia
 */
public class Personaje extends Elemento{
    private int vidas;
    
    public Personaje (ImageIcon imagen, int x, int y){
    super(imagen,x,y);
    }//fin del constructor

    public int getVidas() {
        return vidas;
    }//fin del metodo getVidas

    public void setVidas(int vidas) {
        this.vidas = vidas;
    }//fin del metodo setVidas   
    
    public void ubicacion(){
        setX(20);
        setY(450);
    }
}//fin de la clase
