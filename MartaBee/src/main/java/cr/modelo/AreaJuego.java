/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cr.modelo;

import java.util.ArrayList;
import java.util.Random;
import javax.swing.ImageIcon;

/**
 *
 * @author Diana Artavia
 */
public class AreaJuego {

    private ArrayList<Enemigo> enemigos;
    private ArrayList<Aliado> aliados;
    private Personaje personajePrincipal;
    private Elemento primerVida, segundaVida, terceraVida;
    private Elemento fondo;
    private int puntos;
    private ArrayList<Integer> listaPuntos;
    private int velocidad;
    private int puntosObtenidos;
    
    public AreaJuego() {
        listaPuntos = new ArrayList<>();
        enemigos = new ArrayList<Enemigo>();
        aliados = new ArrayList<Aliado>();
        velocidad = 150;
        setElementos();
    }//fin del constructor

    public void setVelocidad() {
        velocidad = 150;
    }

    public void setElementos() {
        crearEnemigos();
        crearAliados();
        crearVidas();
        crearPersonajePrincipal();
        crearFondo();
    }//fin del metodo crearElementos

    public void crearPersonajePrincipal() {
        ImageIcon martaBee = new ImageIcon("./src/main/resources/abejaUno.gif");
        personajePrincipal = new Personaje(martaBee, 20, 450);
    }//fin del metodo crearPersonajePrincipal

    public Personaje getPersonajePrincipal() {
        return personajePrincipal;
    }//fin del metodo getPersonajePrincipal

    public void crearEnemigos() {
        ImageIcon pajaroEnemigo = new ImageIcon("./src/main/resources/enemigo.gif");
        int num = 0;

        for (int cantidadEnemigos = 0; cantidadEnemigos < 5; cantidadEnemigos++) {
            Enemigo enemigo = new Enemigo(pajaroEnemigo, 0, 0, 0);
            enemigo.ubicacion(num);
            num = num + 1;
            enemigos.add(enemigo);
        }//fin del for
    }//fin del metodo crearEnemigos

    public void crearVidas() {
        ImageIcon vidaUno = new ImageIcon("./src/main/resources/vida.png");
        primerVida = new Elemento(vidaUno, 990, 35);
        ImageIcon vidaDos = new ImageIcon("./src/main/resources/vida.png");
        segundaVida = new Elemento(vidaDos, 1040, 35);
        ImageIcon vidaTres = new ImageIcon("./src/main/resources/vida.png");
        terceraVida = new Elemento(vidaTres, 1090, 35);
    }//fin del metodo crearVidas

    public void crearAliados() {
        Random random = new Random();
        int flor = random.nextInt(5);
        int num = 0;

        for (int cantidadAliados = 0; cantidadAliados < 7; cantidadAliados++) {
            if (flor == 0) {
                ImageIcon florAzul = new ImageIcon("./src/main/resources/azul.gif");
                Aliado aliadoAzul = new Aliado(florAzul, -30, -60, 0);
                aliadoAzul.puntosFlor(flor);
                aliadoAzul.ubicacion(num);
                num = num + 1;
                aliados.add(aliadoAzul);
                flor = 3;
            }

            if (flor == 1) {
                ImageIcon florCeleste = new ImageIcon("./src/main/resources/celeste.gif");
                Aliado aliadoCeleste = new Aliado(florCeleste, -30, -60, 0);
                aliadoCeleste.puntosFlor(flor);
                aliadoCeleste.ubicacion(num);
                num = num + 1;
                aliados.add(aliadoCeleste);
                flor = 0;

            }

            if (flor == 2) {
                ImageIcon florMorada = new ImageIcon("./src/main/resources/morada.gif");
                Aliado aliadoMorado = new Aliado(florMorada, -30, -60, 0);
                aliadoMorado.puntosFlor(flor);
                aliadoMorado.ubicacion(num);
                num = num + 1;
                aliados.add(aliadoMorado);
                flor = 4;
            }

            if (flor == 3) {
                ImageIcon florRosada = new ImageIcon("./src/main/resources/rosada.gif");
                Aliado aliadoRosa = new Aliado(florRosada, -30, -60, 0);
                aliadoRosa.puntosFlor(flor);
                aliadoRosa.ubicacion(num);
                num = num + 1;
                aliados.add(aliadoRosa);
                flor = 2;
            }

            if (flor == 4) {
                ImageIcon florVaricolor = new ImageIcon("./src/main/resources/varicolor.gif");
                Aliado aliadoVaricolor = new Aliado(florVaricolor, -30, -60, 0);
                aliadoVaricolor.puntosFlor(flor);
                aliadoVaricolor.ubicacion(num);
                num = num + 1;
                aliados.add(aliadoVaricolor);
                flor = 1;

            }
        }//fin del for
    }//fin del metodo crearAliados

    public void crearFondo() {
        ImageIcon imagenFondo = new ImageIcon("./src/main/resources/pantallaJuego.png");
        fondo = new Elemento(imagenFondo, 0, 0);
    }//fin del metodo crearFondos

    public Elemento getFondo() {
        return fondo;
    }//fin del metodo getFondos

    public Enemigo getEnemigos(int posicion) {
        return enemigos.get(posicion);
    }//fin del metodo getEnemigos

    public int getCantidadEnemigos() {
        return enemigos.size();
    }//fin del metodo getCantidadEnemigos

    public Elemento getPrimerVida() {
        return primerVida;
    }//fin del metodo getPrimeraVida

    public Elemento getSegundaVida() {
        return segundaVida;
    }//fin del metodo getSegundaVida

    public Elemento getTercerVida() {
        return terceraVida;
    }//fin del metodo getTerceraVida

    public Aliado getAliados(int posicion) {
        return aliados.get(posicion);
    }//fin del metodo getAliados

    public int getCantidadAliados() {
        return aliados.size();
    }//fin del metodo getCantidadAliados

    synchronized public boolean colisionEnemigo() {
        for (int posicion = 0; posicion < enemigos.size(); posicion++) {
            if (enemigos.get(posicion).isColision() && (personajePrincipal.getX() + 60) > enemigos.get(posicion).getX() && personajePrincipal.getX() < (enemigos.get(posicion).getX() + 60) && (personajePrincipal.getY() + 50) > enemigos.get(posicion).getY() && personajePrincipal.getY() < (enemigos.get(posicion).getY() + 60)) {
                personajePrincipal.setVidas(personajePrincipal.getVidas() - 1);
                velocidad -= 50;
                enemigos.get(posicion).ubicacion(posicion);
                return true;
            }//fin del if
        }//fin del for
        return false;
    }//fin del metodo colisonEnemigo

    synchronized public boolean colisionAliado() {
        for (int posicion = 0; posicion < aliados.size(); posicion++) {
            if (aliados.get(posicion).isColision() && (personajePrincipal.getX() + 60) > aliados.get(posicion).getX() && personajePrincipal.getX() < (aliados.get(posicion).getX() + 60) && (personajePrincipal.getY() + 50) > aliados.get(posicion).getY() && personajePrincipal.getY() < (aliados.get(posicion).getY() + 60)) {
                puntos = aliados.get(posicion).getPuntos();
                listaPuntos.add(puntos);
                aliados.get(posicion).ubicacion(posicion);
                velocidad -= 5;
                return true;
            }//fin del if
        }//fin del for
        return false;
    }//fin del metodo colisionAliado

    public void setUbicacion() {
        for (int posicion = 0; posicion < aliados.size(); posicion++) {
            aliados.get(posicion).ubicacion(posicion);
        }//fin del for

        for (int posicion = 0; posicion < enemigos.size(); posicion++) {
            enemigos.get(posicion).ubicacion(posicion);
        }//fin del for

        personajePrincipal.ubicacion();

    }//fin del metodo setUbicacion

    public int puntosTotales() {
        puntosObtenidos = 0;
        for (int posicion = 0; posicion < listaPuntos.size(); posicion++) {
            puntosObtenidos += listaPuntos.get(posicion);
        }//fin del for
        return puntosObtenidos;
    }//fin del metodo puntosTotales

    public void reiniciarPuntos() {
        listaPuntos.clear();
    }

    public int getVelocidad() {
        if (velocidad >= 31) {
            return velocidad;
        }
        return velocidad = 30;
    }//fin del metodo aumentarVelocidad

    public boolean partidaFinalizadas() {
        return personajePrincipal.getVidas() == 0; 
    }//fin del metodo partidaFinalizada

}//fin de la clase
