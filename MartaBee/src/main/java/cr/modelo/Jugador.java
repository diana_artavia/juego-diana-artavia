/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cr.modelo;

/**
 *
 * @author Diana Artavia
 */
public class Jugador {
    private String nombre;
    private String puntuacion;
    
    public static final String[] ETIQUETAS_JUGADOR = {"Nombre", "Puntuación"};

    public Jugador() {
    }//fin del constructor vacio

    public Jugador(String nombre, String puntuacion) {
        this.nombre = nombre;
        this.puntuacion = puntuacion;
    }//fin del constructor con parametros

    public String getNombre() {
        return nombre;
    }//fin del metodo getNombre

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }//fin del metodo setNombre

    public String getPuntuacion() {
        return puntuacion;
    }//fin del metodo getPuntuacion

    public void setPuntuacion(String puntuacion) {
        this.puntuacion = puntuacion;
    }//fin del metodo setPuntuacion
    
    public String getDatosJugador(int indice){
        switch(indice){
            case 0:
                return getNombre();
            case 1:
                return getPuntuacion();
        }//fin del switch
        return null;
    }//fin del metodo getDatosJugador

    @Override
    public String toString() {
        return "Jugador: " + "nombre=" + nombre + ", puntuacion=" + puntuacion;
    }
    
    
    
    
    
    
    
    
}//fin de la clase
